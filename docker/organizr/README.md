## Organizr

## Setup

```bash
# generate variables
./scripts/mkenv
# create the volumes, if local
./scripts/mkvol
# setup the containers
./scripts/up

```



## Configuration

### Initial

```bash
# Create hash key and registration password for initial setup
./scripts/mkkeys
```

1. Install type select your install license `Personal` or `Business`, note `Business` turns off media support
2. Click `Next`
3. Create the superuser user
   1. Username `superuser`
   2. Set an email and password for the account
4. Click `Next`
5. Copy the hash key and registration password generated by the `mkkeys.sh` script
6. Click `Next`
7. Set the `Database Name` as `organizr`
8. Set the `Database Location` as `/config/www/db`
9. Click `Next`
10. Click `Finish`

### Proxy

* Scheme `http`
* Forward Hostname `organizr`
* Forward Port `80`

### IPA Setup

```bash
# create binddn record
./scripts/mkbinddn
# Add binddn to IPA instance
./scripts/installbinddn

```

1. Sign in as the `superuser`
2. Navigate to `System Settings` 
3. Click the `Main` tab
4. Click `Authentication` on the side bar and fill out the following (be sure to change `dc=domain,dc=com` out for your domain and extension):
5. Authentication Type: `Organizr DB + Backend` 
6. Authentication Backend: `Ldap`
7. Host Address: `ldap://ipa:389`
8. Host Base DN: `uid=%s,cn=users,cn=accounts,dc=domain,dc=com`
9. LDAP Backend Type: `Free IPA`
10. Account Prefix: `uid=`
11. Account Suffix: `,cn=users,cn=accounts,dc=domain,dc=com`
12. Bind Username: `uid=organizr,cn=sysaccounts,cn=etc,dc=domain,dc=com`
13. Password: `The password in the organizr-binddn.update file`
14. Enable LDAP TLS: `true`
15. Click `Save`



## Scripts

### installbinddn
Install the binddn record into the FreeIPA instance

### mkkeys
Creates  hash key and registration password

### mkbinddn
Creates a binddn record for FreeIPA

### mkvol
Creates local volumes used for storage by the container

### mkenv
Creates environment variables for use by the containers, don't run this script more than once otherwise it'll overwrite the variables.env file

### up
Creates the containers, volumes, and networks in the stack

### update
Pull the latest container images for the stack

### upgrade
Pull the latest container images for the stack and then set the stack down and bring it back up with the new images

### down
Removes all containers and networks in the stack, but leaves the volumes intact.

