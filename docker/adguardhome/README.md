# Adguard Home



## Setup

```bash
# create the volumes
./scripts/mkvolumes.sh
# setup environment
./scripts/mkenvironment.sh
# setup the containers
./scripts/setup.sh
```



## Configuration

### Initial

1. Setup the reverse proxy using the following:
	* Scheme `http`
	* Forward Hostname / IP `adguardhome`
	* Forward Port `3000`
2. Click `Get Started`
3. Click `Next`
4. Set username and password for administrator
	* Username `superuser`
	* Password generate a password with
      ```bash
         ./scripts/mkpasswd.sh
      ```
5. Click `Next`
6. Setup the router or other devices then click `Next`
7. Change your proxy settings following the section below and then click `Open Dashboard`
	


### Proxy

* Scheme `http`
* Forward Hostname / IP `adguardhome`
* Forward Port `80`



## Scripts


### mkvolumes.sh

Creates volumes used for storage by the container

### mkpasswd.sh

creates a login password

### mkenvironment.sh

Creates environment variables for use by the containers, don't run this script more than once otherwise it'll overwrite the variables.env file

### setup.sh

Creates the containers, volumes, and networks in the stack

### update.sh

Pull the latest container images for the stack

### upgrade.sh

Pull the latest container images for the stack and then set the stack down and bring it back up with the new images

### setdown.sh

Removes all containers and networks in the stack, but leaves the volumes intact.

