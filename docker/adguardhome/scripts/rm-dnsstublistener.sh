#!/bin/bash

echo "Removing DNSStubListener"

sudo mkdir -p /etc/systemd/resolved.conf.d


cat <<EOF |\
  sudo dd status=none of="/etc/systemd/resolved.conf.d/adguardhome.conf"
[Resolve]
DNS=127.0.0.1
DNSStubListener=no
EOF

cd /etc

sudo mv resolv.conf resolv.conf.backup
sudo ln -s ../run/systemd/resolve/resolv.conf resolv.conf

sudo systemctl reload-or-restart systemd-resolved

