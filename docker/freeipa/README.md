# FreeIPA
<img src="freeipa.png" style="zoom:50%;" />

## Setup
```bash
# generate variables
./scripts/mkenv
# create the volumes, if using local
./scripts/mkvol
# create the network
./scripts/mknet
# setup the containers
./scripts/up
```

## Configuration
### Proxy
* Scheme `https`
* Forward Hostname / IP `ipa`
* Forward Port `443`

## Scripts
### mkvol
Creates local volumes used for storage by the container

### mkenv
Creates environment variables for use by the containers, don't run this script more than once otherwise it'll overwrite the .env file

### mknet
Creates the network for use by the IPA server on the local network

### up
Creates the containers, volumes, and networks in the stack

### update
Pull the latest container images for the stack

### upgrade
Pull the latest container images for the stack and then set the stack down and bring it back up with the new images

### down
Removes all containers and networks in the stack, but leaves the volumes intact.
