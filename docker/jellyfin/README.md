# Jellyfin

<img src="jellyfin.png" style="zoom:33%;" />

## Setup

```bash
# generate variables
./scripts/mkenv
# create the local volumes, if using
./scripts/mkvol
# setup the containers
./scripts/up
```


## Configuration

### Initial

1. Select language
2. Setup superuser
   1. Filled in `username` as `superuser`
   2. Create a password for the user
3. Select your media folders which should all be under `/media`
4. Configure remote access
   1. Allow remote connections to this server: `true`
   2. Enable automatic port mapping: `true`

### Proxy

* Scheme `http`
* Forward Hostname / IP `jellyfin`
* Forward Port `8096`

### IPA Setup

```bash
# create binddn record
./scripts/mkbinddn
# Add binddn to IPA instance
./scripts/installbinddn
```

1. Sign in as the `superuser`
2. Click your avatar in the upper right corner of the interface and select `Dashboard`
3. From the left sidebar click `Plugins` under `Advanced`
4. Click `Catalog` from the upper tab menu
5. Under the category `Authentication` click on `LDAP Authentication`
6. Install the newest version
7. Restart jellyfin
8. Navigate back to `Plugins` then click `My Plugins` from the upper tab menu
9. Click on `LDAP-Auth`
10. Fill out the following (be sure to change `dc=domain,dc=com` out for your domain and extension):
    * LDAP Server: `ipa`
    * Secure LDAP: `true`
    * STARTTLS: `false`
    * Skip SSL/TLS Verification: `true`
    * LDAP Base DN for searches: `dc=domain,dc=com`
    * LDAP Port: `636`
    * LDAP Attributes: `uid, cn, mail, displayName`
    * LDAP Name Attribute: `uid`
    * LDAP User Filter: `(&(objectClass=person)(memberof=cn=jellyfin,cn=groups,cn=accounts,dc=domain,dc=com))`
    * LDAP Admin Filter: `(&(objectClass=person)(memberof=cn=jellyfin_admin,cn=groups,cn=accounts,dc=domain,dc=com))`
    * LDAP Bind User: `uid=jellyfin,cn=sysaccounts,cn=etc,dc=domain,dc=com`
    * LDAP Bind User Password: 
    * Enable User Creation
11. Click save
12. Restart jellyfin


### Authentik Setup

#### Authentik SSO
1. Login as akadmin with the password you set
2. Click on `Admin interface` in the top bar
3. From the left sidebar select `Applications > Providers`
4. Click `Create`
5. Select `OAuth2/OpenID Provider`
6. Click `Next`
7. Fill in the following (replacing domain.com with your own)
   * Name `Jellyfin OAuth`
   * Authorization flow `default-provider-authoriztion-implicit-consent`
   * Client type `Confidential`
   * Client ID `This will be generated`
   * Client Secret `This will be generated`
   * Redirect URIs/Origins (RegEx) `https://jellyfin.domain.com/sso/OID/r/authentik`
   * Signing Key `self-signed is fine here`
   * Scopes
      * authentik default OAuth Mapping: OpenID 'email'
      * Group Membership
      * authentik default OAuth Mapping: OpenID 'openid'
      * authentik default OAuth Mapping: OpenID 'profile'
   * Subject mode `Based on the User's username`
8. Click `Finish`
9. From the left sidebar select `Applications > Applications`
10. Click `Create`
11. Fill in the following (replacing domain.com with your own)
   * Name `Jellyfin`
   * Slug `jellyfin`
   * Provider `Jellyfin OAuth`
   * Policy engine mode `any`
   * Launch URL `https://jellyfin.domain.com/sso/OID/p/authentik`

#### Authentik LDAP
1. Login as akadmin with the password you set
2. Click on `Admin interface` in the top bar
3. From the left sidebar select `Applications > Providers`
4. Click `Create`
5. Select `LDAP Provider`
6. Click `Next`
7. Fill in the following (replace domain.com with your own domain)
   * Name `Jellyfin LDAP`
   * Bind Flow `ldap-authentication-flow`
   * Search group `ldapsearch`
   * Bind mode `Cached binding`
   * Search mode `Cached querying`
   * Base DN `DC=ldap,DC=domain,DC=com`
   * TLS Server name  `authentik.domain.com`
8. Click `Finish`
9. From the left sidebar select `Applications > Applications`
10. Click the edit button on `Jellyfin`
11. Under `Backchannel providers` Select `Jellyfin LDAP`
12. Click `Update`
13. Navigate to `Applications > Outputs`
14. Click edit on the LDAP outpost which is on the same network as `jellyfin` in example `Proxy_LDAP`
15. Select `Jellyfin (Jellyfin LDAP)` in addition to your other LDAP applications already there
16. Click `Update`
17. Navigate to `Directory > Users` from the left sidebar
18. Click `Create Service account`
19. Fill in the following
   * Username `jellyfin_ldap`
   * Create group `false`
   * Expiring `false`
20. Click `Create`
21. Record the `username` and `password`
22. Click on the `jellyfin_ldap` user
23. Click on the `Groups` tab
24. Click `Add to existing group`
25. Click `+` button
26. Check `ldapsearch`
27. Click `Add` and click `Add` again

#### Jellyfin SSO
1. Login to Jellyfin as the `superuser`
2. Click the person icon in the upper right
3. Click `Dashboard`
4. Click `Plugins` from the sidebar
5. Click the `Repositories` tab
6. Click the `+` button to add a new repository
7. Fill in
   * Repository Name `Jellyfin SSO Plugin`
   * Repository URL `https://raw.githubusercontent.com/9p4/jellyfin-plugin-sso/manifest-release/manifest.json`
8. Click `Save`
9. Click the `Catalog` tab
10. Click `SSO Authentication`
11. Click `Install`
12. Click `Got it`
13. Restart jellyfin
14. Navigate to `Plugins`
15. Click on `SSO-Auth`
16. Under `Add /Update Provider Configuration` fill in the following
   * Name of OID Provider `authentik` this is the same authentik in the Redirect URIs/Origins (RegEx) from before
   * OID Endpoint `https://authentik.domain.com/application/o/jellyfin/` note jellyfin here is the slug from Authentik
   * OpenID Client ID `Client ID from Authentik`
   * OID Secret `Client Secret from Authentik`
   * Enabled `true`
   * Enable Authorization by Plugin `true`
   * Enable All Folders `your choice`
   * Enabled Folders `Select what you want if you said false to the above`
   * Roles `jellyfin`
   * Admin Roles `jellyfin_admin`
   * Role Claim `groups`
   * Request Additional Scopes `groups`
   * Set default Provider `Jellyfin.Plugin.LDAP_Auth.LdapAuthenticationProviderPlugin`

##### SSO Button
1. Login to Jellyfin as the `superuser`
2. Click the person icon in the upper right
3. Click `Dashboard`
4. Click `General`
5. Fill in and modify as desired (replace domain.com with your own domain)
   * Login disclaimer
      ```html
      <a href="https://jellyfin.domain.com/sso/OID/p/authentik" class="raised cancel block emby-button">
         Sign in with SSO
      </a>
      ```
   * Custom CSS code
      ```css
         a.raised.emby-button {
            padding: 0.9em 1em;
            color: inherit !important;
         }
         .disclaimerContainer {
            display: block;
         }
      ```

#### Jellyfin LDAP
1. Sign in as the `superuser`
2. Click your avatar in the upper right corner of the interface and select `Dashboard`
3. From the left sidebar click `Plugins` under `Advanced`
4. Click `Catalog` from the upper tab menu
5. Under the category `Authentication` click on `LDAP Authentication`
6. Install the newest version
7. Restart jellyfin
8. Navigate back to `Plugins` then click `My Plugins` from the upper tab menu
9. Click on `LDAP-Auth`
10. Fill out the following (be sure to change `dc=domain,dc=com` out for your domain and extension):
   * LDAP Server: `authentik_proxy_ldap`
   * LDAP Port: `6636`
   * Secure LDAP: `true`
   * STARTTLS: `false`
   * Skip SSL/TLS Verification: `true`
   * LDAP Bind User: `cn=jellyfin_ldap,ou=users,DC=ldap,DC=domain,DC=com`
   * LDAP Bind User Password: `password from authentik`
   * LDAP Base DN for searches: `DC=ldap,DC=domain,DC=com`
11. Click `Save and Test Server Settings`
   * LDAP Search Filter: `(&(objectClass=user)(memberof=cn=jellyfin,ou=groups,dc=ldap,dc=domain,dc=com))`
   * LDAP Admin Filter: `(&(objectClass=user)(memberof=cn=jellyfin_admin,ou=groups,dc=ldap,dc=domain,dc=com))`
   * LDAP Attributes: `uid, cn, mail, displayName`
   * LDAP Name Attribute: `cn`
12. Click `Save and Test LDAP Filter Settings`
   * Enable User Creation `true`
   * Select which ever library settings
11. Click save
12. Restart jellyfin

## Scripts

### installbinddn
Install the binddn record into the FreeIPA instance

### installgroups
Install groups into the FreeIPA instance

### mkbinddn
Creates a binddn record for FreeIPA

### mkvol
Creates local volumes used for storage by the container

### mkenv
Creates environment variables for use by the containers, don't run this script more than once otherwise it'll overwrite the .env file

### up
Creates the containers, volumes, and networks in the stack

### update
Pull the latest container images for the stack

### upgrade
Pull the latest container images for the stack and then set the stack down and bring it back up with the new images

### down
Removes all containers and networks in the stack, but leaves the volumes intact.
