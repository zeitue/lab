# Drone

## Preperation
* Login to your Gitea instance
* Click your avatar in the upper right corner
* Click `Settings`
* Click on `Applications` from the top
* Under `Manage OAuth2 Applications` fill out the following changing domain.com for your domain and extension
  * Application Name `Drone`
  * Redirect URI `https://drone.domain.com/login`
* Click `Create Application`
* Record the information given to use later when running `mkenvironment.sh` script

## Setup

```bash
# create the volumes
./scripts/mkvolumes.sh
# generate variables
./scripts/mkenvironment.sh
# setup the containers
./scripts/setup.sh
```



## Configuration

### Proxy

* Scheme `http`
* Forward Hostname / IP `drone`
* Forward Port `80`


## Scripts

### mkvolumes.sh

Creates volumes used for storage by the container

### mkenvironment.sh

Creates environment variables for use by the containers, don't run this script more than once otherwise it'll overwrite the variables.env file

### setup.sh

Creates the containers, volumes, and networks in the stack

### update.sh

Pull the latest container images for the stack

### upgrade.sh

Pull the latest container images for the stack and then set the stack down and bring it back up with the new images

### setdown.sh

Removes all containers and networks in the stack, but leaves the volumes intact.

