#!/bin/bash

echo -n 'Enter your Gitea URL (gitea.domain.com): '
read DRONE_GITEA_SERVER

echo -n 'Enter your Gitea client ID: '
read DRONE_GITEA_CLIENT_ID

echo -n 'Enter your Gitea secret: '
read DRONE_GITEA_CLIENT_SECRET

echo -n 'Enter your new Drone URL (drone.domain.com): '
read YOUR_DRONE_URL

YOUR_DRONE_SECRET=$(openssl rand -hex 16)

echo -n 'How many drone runners to allow at once(1-1000): '
read YOUR_RUNNER_CAP

sed s/DRONE_GITEA_SERVER/$DRONE_GITEA_SERVER/g example.env |\
sed s/DRONE_GITEA_CLIENT_ID/$DRONE_GITEA_CLIENT_ID/g |\
sed s/DRONE_GITEA_CLIENT_SECRET/$DRONE_GITEA_CLIENT_SECRET/g |\
sed s/YOUR_DRONE_URL/$YOUR_DRONE_URL/g |\
sed s/YOUR_RUNNER_CAP/$YOUR_RUNNER_CAP/g |\
sed s/YOUR_DRONE_SECRET/$YOUR_DRONE_SECRET/g > .env
