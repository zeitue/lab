



# Gitea

<img src="./gitea.png" style="zoom: 25%;" />

## Setup

```bash
# create the local volumes
./scripts/mkvol
# generate variables
./scripts/mkenv
# setup the containers
./scripts/up
```



## Configuration

### Initial

1. Leave the `Database Settings` alone
2. Under `General Settings`
   1. Set the `Site Title` to your company name
   2. Change `SSH Server Domain` to the address of the server
   3. Change `Gitea Base URL` to the address of your server with protocol and port:  `https://subdomain.domain.com`
   4. Set the `SSH Server Port` to the ssh port you chose when setting in `mkenv` for example `22200`
3. Under `Optional Settings`
   1. Setup Email if wanted
   2. Under `Administrator Account Settings`
      1. Filled in `Administrator Username` as `superuser`
      2. Create a password for the user
      3. Assign the user an email address

### Proxy

* Scheme `http`
* Forward Hostname / IP `gitea`
* Forward Port `3000`

### IPA Setup

```bash
# create binddn record
./scripts/mkbinddn
# Add binddn to IPA instance
./scripts/installbinddn
# Add groups to IPA instance
./scripts/installgroups
```

1. Sign in as the `superuser`
2. Click your avatar in the upper right corner of the interface and select `Site Administration`
3. Click `Authentication Source`
4. Click `Add Authentication Source` and fill out the following (be sure to change `dc=domain,dc=com` out for your domain and extension):
   * Authentication Type: `LDAP (via BindDN) `
   * Authentication Name: `FreeIPA`
   * Security Protocol: `LDAPS`
   * Host: `ipa`
   * Port: `636`
   * Bind DN:  `uid=gitea,cn=sysaccounts,cn=etc,dc=domain,dc=com`
   * Bind Password: `The password in the gitea-binddn.update file`
   * User Search Base: `cn=accounts,dc=domain,dc=com`
   * User Filter: `(&(objectClass=person)(uid=%s)(memberof=cn=gitea,cn=groups,cn=accounts,dc=domain,dc=com))`
   * Admin Filter: `(&(objectClass=person)(memberof=cn=gitea_admins,cn=groups,cn=accounts,dc=domain,dc=com))`
   * Username Attribute: `uid`
   * First Name Attribute: `givenName`
   * Surname Attribute: `sn`
   * Email Attribute: `mail`
   * Public SSH Key Attribute: `ipaSshPubKey`
   * Fetch Attributes in Bind DN Context: `true`
   * Skip TLS Verify: `true`
   * Enable User Syncronization: `true`
   * This Authentication Source is Activated: `true`



## Scripts
### installbinddn
Install the binddn record into the FreeIPA instance

### mkbinddn
Creates a binddn record for FreeIPA

### installgroups
Install groups into the FreeIPA instance

### mkvol
Creates local volumes used for storage by the container

### mkenv
Creates environment variables for use by the containers, don't run this script more than once otherwise it'll overwrite the variables.env file

### up
Creates the containers, volumes, and networks in the stack

### update
Pull the latest container images for the stack

### upgrade
Pull the latest container images for the stack and then set the stack down and bring it back up with the new images

### down
Removes all containers and networks in the stack, but leaves the volumes intact.
