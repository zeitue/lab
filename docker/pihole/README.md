# Pi-hole
<img src="./pi-hole.png" style="zoom: 50%;" />

## Setup

```bash
# generate variables
./scripts/mkenv
# create the local volumes
./scripts/mkvol
# setup the containers
./scripts/up
```

### Proxy

* Scheme `http`
* Forward Hostname / IP `pihole`
* Forward Port `80`

## Scripts
### mkvol
Creates local volumes used for storage by the container

### mkenv
Creates environment variables for use by the containers, don't run this script more than once otherwise it'll overwrite the variables.env file

### up
Creates the containers, volumes, and networks in the stack

### update
Pull the latest container images for the stack

### upgrade
Pull the latest container images for the stack and then set the stack down and bring it back up with the new images

### down
Removes all containers and networks in the stack, but leaves the volumes intact.

### rm-dnsstublistener
Resolves conflict with the DNS stub listener.


