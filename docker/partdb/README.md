# Part-DB
<img src="partdb.png" style="zoom:50%;" />

## Setup
```bash
# generate variables
# note if you want to use SAML be sure to set that up before running this script
# for information on how to setup SAML with Authentik see the Authentik section
# for what to fill in while running mkenv see notes below
./scripts/mkenv
# create the volumes, if using local
./scripts/mkvol
# setup the containers
./scripts/up
# migrate database
./scripts/migrate
# Add groups, note if you are not using IPA as a backend you will have to create these in Authentik/IdP yourself
./scripts/installgroups
```

### Notes
* Enter your SAML IDP entity ID: `https://authentik.domain.com`
* Enter your SAML IDP SingleSignOnService: `https://authentik.domain.com/application/saml/part-db/sso/binding/redirect/`
* Enter your SAML IDP SingleLogoutService: `https://authentik.domain.com/application/saml/part-db/slo/binding/redirect/`
* Enter your SAML IDP X509 certificate: `copy the result of using flattenkey on the certificate you downloaded in the Authentik section`
* Enter your SAML SP X509 certificate: `copy the result of using flattenkey on the certificate you downloaded in the Authentik section`
* Enter your SAML SP private key: `copy the result of using flattenkey on the private key you downloaded in the Authentik section`


## Proxy
* Scheme `http`
* Forward Hostname / IP `partdb`
* Forward Port `80`

## Authentik
Login to the administration interface

### Generate Certificate
1. Navigate to `System > Certificates`
2. Click `Generate`
3. Name it `Part-DB` and set the duration you want it to be valid for
4. Click `Generate`
5. Download the `Certificate` and `Private Key` for later

### Create Property Mappings
1. Navigate to `Customization > Property Mappings`
2. Click `Create`
3. Select `SAML Property Mapping`
4. Click `Next`
5. Fill in the following:
    * Name `x500 email`
    * SAML Attribute Name `urn:oid:1.2.840.113549.1.9.1`
    * Expression
        ```python
        return request.user.email
        ```
6. Click Finish
7. Click `Create`
8. Select `SAML Property Mapping`
9. Click `Next`
10. Fill in the following:
    * Name `x500 givenName`
    * SAML Attribute Name `urn:oid:2.5.4.42`
    * Expression
        ```python
        if len(request.user.name) > 0:
            return request.user.name.split()[0]
        else:
            return ''
        ```
11. Click Finish
12. Click `Create`
13. Select `SAML Property Mapping`
14. Click `Next`
15. Fill in the following:
    * Name `x500 surname`
    * SAML Attribute Name `urn:oid:2.5.4.4`
    * Expression
        ```python
        n = request.user.name
        if n.rfind(' ') < len(n) - 1:
            return n.split()[1]
        else:
            return ''
        ```
16. Click Finish
17. Click `Create`
18. Select `SAML Property Mapping`
19. Click `Next`
20. Fill in the following:
    * Name `group`
    * SAML Attribute Name `group`
    * Expression
        ```python
        for group in user.ak_groups.all():
            yield group.name
        ```
21. Click Finish

### Create Provider
1. Navigate to `Applications > Providers`
2. Click `Create`
3. Click `SAML Provider`
4. Fill in the following (replacing domain.com with your own)
    * Name `Part-DB SAML`
    * Authorization flow `default-provider-authorization-implicit-consent (Authorize Application)`
    * ACS URL `https://partdb.domain.com/saml/acs`
    * Issuer `https://authentik.domain.com`
    * Service Provider Binding `Post`
    * Audience `https://partdb.domain.com/sp`
    * Signing Certificate `Part-DB`
    * Verification Certificate `Part-DB`
    * Property mappings
        * group
        * x500 email
        * x500 surname
        * x500 givenName
    * NameID Property Mapping `authentik default SAML Mapping: Username`
5. Click `Finish`

### Create Application
1. Navigate to `Applications > Applications`
2. Click `Create`
3. Fill in the following (replacing domain.dom with your own)
    * Name `Part-DB`
    * Slut `part-db`
    * Provider `Part-DB SAML`
    * Launch URL `https://partdb.domain.com/saml/login`
    * Open in new tab `true`
4. Click `Finish`


## Scripts
### mkvol
Creates local volumes used for storage by the container

### mkenv
Creates environment variables for use by the containers, don't run this script more than once otherwise it'll overwrite the .env file

### up
Creates the containers, volumes, and networks in the stack

### update
Pull the latest container images for the stack

### upgrade
Pull the latest container images for the stack and then set the stack down and bring it back up with the new images

### down
Removes all containers and networks in the stack, but leaves the volumes intact.

## console
Access application console

## installgroups
Install FreeIPA groups

## migrate
Migrate application database

## flattenkey
Use this script to reduce your certificates and private keys down to single lines
