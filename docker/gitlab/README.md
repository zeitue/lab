# Gitlab
<img src="./gitlab.png" style="zoom: 80%;" />

## Intial Setup
1. Navigate to `gitlab.domain.com` replacing `domain.com` with your own
2. Run `./scripts/getrootpasswd`
3. Login with username `root` and the password from the previous step
4. Click `Configure GitLab`

