# Portainer

<img src="portainer.png" style="zoom:100%;" />

## Setup
``` bash
# generate variables
./scripts/mkenv
# create the local volumes, if using
./scripts/mkvol
# setup the containers
./scripts/up
```

## Configuration

### Account creation
1. open http://127.0.0.1:9000 or http://`IP address of host`:9000
2. Set the `Username` as `superuser`
3. Set the password and confirm it.

### Connection setup
You'll need connect Portainer to the container environment you want to manage.

1. Select Docker
2. Click `Connect`

### IPA setup
```bash
# Create binddn record
./scripts/mkbinddn
# Add binddn to IPA instance
./scripts/installbinddn
# Add groups
./scripts/installgroups
```

1. In the Portainer app to do `Settings → Authentication`
2. Click LDAP
3. Fill out the following (be sure to change `dc=domain,dc=com` out for your domain and extension):
   - LDAP Server: `ipa:389`
   - Anonymous mode: `false`
   - Use StartTLS: `true`
   - Reader DN: `uid=portainer,cn=sysaccounts,cn=etc,dc=domain,dc=com`
   - Password: `The password in the portainer-binddn.update file`
   - Skip verification of server certificate: `true`
   - Automatic user provisioning: `true`
   - Base DN: `dc=domain,dc=com`
   - Username attribute: `uid`
   - Filter: `(&(objectClass=person)(memberof=cn=portainer,cn=groups,cn=accounts,dc=domain,dc=com))`
   - Group Base DN: `cn=groups,cn=accounts,dc=domain,dc=com`
   - Group Membership Attribute: `cn`
   - Group Filter: `(objectClass=groupofnames)`
4. Click `Save settings`

### Proxy

* Scheme `http`
* Forward Hostname / IP `portainer`
* Forward Port `9000`

## Scripts

### installbinddn
Install the binddn record into the FreeIPA instance

### installgroups
Install groups into the FreeIPA instance

### mkbinddn
Creates a binddn record for FreeIPA

### mkvol
Creates volumes used for storage by the container

### mkenv
Creates environment variables for use by the containers, don't run this script more than once otherwise it'll overwrite the .env file

### up
Creates the containers, volumes, and networks in the stack

### update
Pull the latest container images for the stack

### upgrade
Pull the latest container images for the stack and then set the stack down and bring it back up with the new images

### down
Removes all containers and networks in the stack, but leaves the volumes intact.
