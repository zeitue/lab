#!/bin/bash

mkdir -p ./config
mkdir -p ./plugins
mkdir -p ./videos
mkdir -p ./database
mkdir -p ./custom-autoload
mkdir -p ./tensorflow
