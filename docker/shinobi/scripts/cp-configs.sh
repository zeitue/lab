#!/bin/bash

docker cp shinobi:/home/Shinobi/conf.sample.json shinobi:/config/conf.json
docker cp shinobi:/home/Shinobi/super.sample.json shinobi:/config/super.json
jq '. += {"allowLdapSignOn":true,}' config/conf.json > /tmp/conf.json
sudo mv /tmp/conf.json config/conf.json
