#!/bin/bash
echo -n "Enter your domain: "
read YOUR_DOMAIN

echo "Generating plugin key"
KEY=$(head -c 1024 < /dev/urandom | sha256sum | awk '{print substr($1,1,29)}')

sed s/YOUR_TESNORFLOW_KEY/$KEY/g example.env |\
sed s/YOUR_DOMAIN/$YOUR_DOMAIN/g  > .env
