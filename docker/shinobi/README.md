# Shinobi

<img src="./shinobi.png" style="zoom: 100%;" />

## Setup

```bash
# create the volumes
./scripts/mkvolumes.sh
# generate variables
./scripts/mkenvironment.sh
# setup the containers
./scripts/setup.sh
# copy configuration files
./scripts/cp-configs.sh
```



## Configuration

### Initial

* Navigate to: http://YOUR_URL/super and login with:
  * Username `admin@shinobi.video`
  * Password `admin`
* Click on Preferences
* Fill in a new  email  and password
* Then click `Save`
* Click on `Accounts` from the top navigation
* Then click `Add`
* Fill out an email and password for your `superuser` account, and for the group key you can use your organization
* Set the `Max Storage Amount` in MB for all stored video
* Then click `Save`
* Log out
* Login to your `superuser` at http://YOUR_URL/admin
* Click the email address in the upper left hand corner
* Click on `Sub-Account Manager`
  * Fill in each account's email address and password
  * Set the privileges you want to allow the account to have
  * Click `Add New`
  * Repeat for each new user
  * Log out
* You should now be able to login as any of the sub-accounts


### Proxy

* Scheme `http`
* Forward Hostname / IP `shinobi`
* Forward Port `8080`

## Scripts

### cp-configs.sh

Copy sample configurations to modify

### mkvolumes.sh

Creates volumes used for storage by the container

### mkenvironment.sh

Creates environment variables for use by the containers, don't run this script more than once otherwise it'll overwrite the variables.env file

### setup.sh

Creates the containers, volumes, and networks in the stack

### update.sh

Pull the latest container images for the stack

### upgrade.sh

Pull the latest container images for the stack and then set the stack down and bring it back up with the new images

### setdown.sh

Removes all containers and networks in the stack, but leaves the volumes intact.

