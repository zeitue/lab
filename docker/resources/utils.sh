custom() {
    echo -n "Do you want to add your own changes in 'compose/custom.yml'? (y/n): "
    read CUSTOM
    if [ "$CUSTOM" == "y" ] || [ "$CUSTOM" == "Y" ]; then
        YOUR_BUILD="${YOUR_BUILD}:compose/custom.yml"
        touch compose/custom.yml
    fi
    unset CUSTOM
}


prefix() {
    echo -n "Do you want to prefix the compose setup? (y/n): "
    read IN
    if [ "$IN" == "y" ] || [ "$IN" == "Y" ]; then
        echo -n "Enter your prefix: "
        read PREFIX
        PREFIX="${PREFIX}_"
    fi
    unset IN
}

# $1 = PROMPT
# $2 = DIR, set this to the default before calling
# $3 = IP
# $4 = BUILD_EXT
volume() {
    local PROMPT="$1"
    local -n DIR="$2"
    local -n IP="$3"
    local BUILD_EXT="$4"
    echo "You can use NFS, (abs)olute path or (rel)ative path"
    PS3="Which do you want to use for ${PROMPT}?: "
    select choice in "NFS" "Absolute Path" "Relative Path"
    do
        case $choice in
            "NFS")
                YOUR_BUILD="${YOUR_BUILD}:compose/nfs/${BUILD_EXT}.yml"
                echo -n "Enter NFS path to your ${PROMPT} directory: "
                read IN
                IFS=':' read -ra ADDR <<< "$IN"
                IP=${ADDR[0]}
                DIR=${ADDR[1]}
                break;;
            "Absolute Path")
                YOUR_BUILD="${YOUR_BUILD}:compose/local/${BUILD_EXT}.yml"
                echo -n "Enter the absolute path to your ${PROMPT} directory: "
                read DIR
                break;;
            "Relative Path")
                YOUR_BUILD="${YOUR_BUILD}:compose/local/${BUILD_EXT}.yml"
                echo "Enter your ${PROMPT} directory relative to \"${PWD}\""
                echo "Default location is ${DIR}"
                echo -n "Enter your ${PROMPT} directory: "
                read IN
                if [ ! -z "$IN" ]; then
                    DIR="\${PWD}/$IN"
                fi
                break;;
            *)
            echo "Invalid response";;
        esac
    done
}

gpu() {
    local COMPOSE_ROOT="$1"
    echo "Do you want to add a GPU?"
    PS3="Which do you want to use?: "
    select choice in "None" "Intel" "NVIDIA" "AMD"
    do
        case $choice in
            "None")
            break;;
            "Intel")
            YOUR_BUILD="${YOUR_BUILD}:${COMPOSE_ROOT}/intel.yml"
            break;;
            "NVIDIA")
            YOUR_BUILD="${YOUR_BUILD}:${COMPOSE_ROOT}/nvidia.yml"
            break;;
            "AMD")
            YOUR_BUILD="${YOUR_BUILD}:${COMPOSE_ROOT}/amd.yml"
            break;;
            *)
            echo "Invalid response";;
        esac
    done
}

timezone() {
    local -n YTZ=$1
    if [ -f /etc/timezone ]; then
        OLSONTZ=`cat /etc/timezone`
    elif [ -h /etc/localtime ]; then
        OLSONTZ=`readlink /etc/localtime | sed "s/\/usr\/share\/zoneinfo\///"`
    else
        checksum=`md5sum /etc/localtime | cut -d' ' -f1`
        OLSONTZ=$(find /usr/share/zoneinfo -type f ! -name 'posixrules'\
            -exec cmp -s {} /etc/localtime \; -print |\
            sed -e 's@.*/zoneinfo/@@' |\
            head -n1 |\
            sed -e "s/^posix\///" |\
            sed -e "s/^Etc\///"
        )
    fi
    YTZ="$OLSONTZ"
}