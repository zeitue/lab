# Kroki
<img src="./kroki.svg" style="zoom: 30%;" />

## Setup

```bash
# create environment variables
./scripts/mkenv
# start containers
./scripts/up
```

## Scripts
### mkenv
Creates environment variables for use by the containers, don't run this script more than once otherwise it'll overwrite the .env file

### up
Creates the containers, volumes, and networks in the stack

### update
Pull the latest container images for the stack

### upgrade
Pull the latest container images for the stack and then set the stack down and bring it back up with the new images

### down
Removes all containers and networks in the stack, but leaves the volumes intact.

