# Authentik

<img src="./authentik.png" style="zoom: 25%;" />

```bash
# generate variables
./scripts/mkenv
# create the local volumes
./scripts/mkvol
# setup the containers
./scripts/up
```

## Initial Setup
1. Navigate to https://authentik.domain.com/if/flow/initial-setup/ (replace domain.com with your own domain)
2. Fill in
    * Email `The email of the administrator`
    * Password `Password for administrator`
3. This will create the admin user with the username `akadmin`
4. Click the gear in the upper right of the top bar
5. Click `MFA Devices` from the side bar
6. Select one of the options from the `Enroll` menu
> Always multi factor your accounts, espectially your administrator accounts.

### IPA Setup
```bash
# create binddn record
./scripts/mkbinddn
# Add binddn to IPA instance
./scripts/installbinddn
```
1. Login as akadmin with the password you set
2. Click on `Admin interface` in the top bar
3. From the left sidebar select `Directory > Federation & Social Login`
4. Click `Create`
5. Select `LDAP Source`
6. Click `Next`
7. Fill in the following (replacing domain.com with your domain)
    * Name `IPA`
    * Slug `ipa`
    * Enabled `true`
    * Sync users `true`
    * User password writeback `true`
    * Sync groups `true`
    * Server URI `ldap://ipa`
    * Enable StartTLS `true`
    * Bind CN `uid=authentik,cn=sysaccounts,cn=etc,dc=domain,dc=com`
    * Bind Password `The password in the authentik-binddn.update file`
    * Base DN `dc=domain,dc=com`
    * User Property Mappings
        * authentik default LDAP Mapping: mail
        * authentik default LDAP Mapping: Name
        * authentik default Active Directory Mapping: givenName
        * authentik default Active Directory Mapping: sn
        * authentik default OpenLDAP Mapping: cn
        * authentik default OpenLDAP Mapping: uid
    * Group Property Mappings
        * authentik default OpenLDAP Mapping: cn
    * User path `users`
    * Addition User DN `cn=users,cn=accounts`
    * Addition Group DN `cn=groups,cn=accounts`
    * User object filter `(objectClass=person)`
    * Group object filter `(objectClass=groupofnames)`
    * Group membership field `member`
    * Object uniqueness field `ipaUniqueID`
8. Click Finish
9. Click on `IPA`
10. Click `Run sync again`


### Google Setup
#### Google
1. Visit https://console.developers.google.com/ to create a new project
2. Create a New project.
3. Fill in the following
    * Project Name `Choose a name`
    * Organization `Leave as default if unsure`
    * Location `Leave as default if unsure`
4. Click `Create`
5. Choose your project from the drop down at the top
6. Click the Credentials menu item on the left. It looks like a key.
7. Click on Configure Consent Screen
8. User Type: If you do not have a Google Workspace (GSuite) account choose External. If you do have a Google Workspace (Gsuite) account and want to limit access to only users inside of your organization choose Internal
9. Fill in the following
    * App Name `Choose an Application name`
    * User Support Email `Must have a value`
    * Authorized Domains `authentik.domain.com`
    * Developer Contact Info `Must have a value`
10. Click Save and Continue
11. Click the Credentials menu item on the left sidebar
12. Click `Create Credentials` on the top of the screen
13. Choose `OAuth Client ID`
14. Fill in the following (replacing domain.com with your own domain)
    * Application Type `Web Application`
    * Name `Choose a name`
    * Authorized redirect URIs `https://authenik.domain.com/source/oauth/callback/google/`
15. Click `Create`
16. Copy and store Your Client ID and Your Client Secret for later

#### Authentik
1. Login as akadmin with the password you set
2. Click on `Admin interface` in the top bar
3. From the left sidebar select `Directory > Federation & Social Login`
4. Click `Create`
5. Select `Google OAuth Source`
6. Fill in the following
    * Name `Google` (note this can be whatever)
    * Slug `google` (note this can be whatever)
    * User matching mode `Link user with identical email address.` only use this method if email is verified
    * Consumer key `Client ID from Google`
    * Consumer secret `Client Secret from Google`
7. Click `Finish`
8. Navigate to `Flows & Stages > Flows`
9. Click on `default-source-enrollment`
10. Click `Stage Bindings` from the tabs
11. Uncollapse `default-source-enrollment-prompt`
12. Click `Create & bind Policy`
13. Select `Expression Policy`
14. Click `Next`
15. Fill in the following
    * Name `google-username`
    * Expression
    ```python
        email = request.context["prompt_data"]["email"]
        # request.context["prompt_data"]["username"] = email
        request.context["prompt_data"]["username"] = email.split("@")[0]
        return False
    ```
16. Click `Next`
17. Fill in the following
    * Order `0`
    * Timeout `30`
18. Click `Finish`
19. Click `Edit Binding` on `Policy default-source-enrollment-if-username`
20. Change `Order` to `1` and click `Update`
21. Navigate to `Flows & Stages > Flows`
22. Click on `default-authentication-flow`
23. Click the `Stage Bindings` tab
24. Click `Edit Stage` for the `default-authentication-identification` stage
25. Under Sources you should see the additional sources you have configured. Select all applicable sources to have them displayed on the Login Page
26. Click `Update`

### LDAP
1. Login as akadmin with the password you set
2. Click on `Admin interface` in the top bar
3. From the left sidebar select `Directory > Groups`
4. Click `Create`
5. Fill in `ldapsearch` for the name
6. Click `Create`
7. Navigate to `Flows & Stages > Stages`
8. Click `Create`
9. Select `Password Stage`
10. Fill in `ldap-authentication-password` for the name
11. Select all `Backends`
12. Click `Finish`
13. Click `Create`
14. Select `Identification Stage`
15. Fill in `ldap-identification-stage` for the name
16. Select `Username` and `Email` from `User fields`
17. Select `ldap-authentication-password` for `Password Stage`
18. Select `authentik Built-in` for `Sources`
19. Click `Finish`
20. Click `Create`
21. Select `User Login Stage`
22. Fill in `ldap-authentication-login` for the name
23. Click `Finish`
24. Navigate to `Flows & Stages > Flows`
25. Click `Create`
26. Fill in the following
    * Name `ldap-authentication-flow`
    * Title `ldap-authentication-flow`
    * Slug `ldap-authentication-flow`
    * Destination `Authentication`
    * Authentication `No requirements`
    * Compatibility mode `on`
27. Click `Create`
28. Click `ldap-authentication-flow`
29. Click `Stage Bindings`
30. Click `Bind existing stage`
31. Fill in
    * Stage `ldap-identification-stage`
    * Order `10`
32. Click `Create`
33. Click `Bind existing stage`
34. Fill in
    * Stage `ldap-authentication-login`
    * Order `30`
35. Click `Create`
36. Navigate to `Applications > Providers`
37. Click `Create`
38. Select `LDAP Provider`
39. Click `Next`
40. Fill in (replace domain.com with your own)
    * Name `LDAP`
    * Bind flow `ldap-authentication-flow`
    * Search group `ldapsearch`
    * Base DN `dc=ldap,dc=domain,dc=com`
    * TLS Server name `authentik.domain.com`
41. Click `Finish`
42. Navigate to `Applications > Applications`
43. Click `Create`
44. Fill in
    * Name `LDAP`
    * Slug `ldap`
    * Provider `LDAP`
45. Click `Create`
46. Navigate to `Applications > Outposts`
47. Click `Create`
48. Fill in
    * Name `LDAP`
    * Type `LDAP`
    * Inegration `Local Docker connection`
    * Applications `LDAP (LDAP)`
49. Under Configuration locate `docker_network` and set it to `proxy`
50. Under Configuration locate `object_naming_template` and set it to `authentik_%(name)s`
51. Click `Create`
> Note a new docker container will spawn with the name `authentik_%(name)s` with ports for ldap 3389 and ldaps 6636 on the `proxy` network

## Scripts
### mkvol
Creates local volumes used for storage by the container

### mkenv
Creates environment variables for use by the containers, don't run this script more than once otherwise it'll overwrite the .env file

### up
Creates the containers, volumes, and networks in the stack

### update
Pull the latest container images for the stack

### upgrade
Pull the latest container images for the stack and then set the stack down and bring it back up with the new images

### down
Removes all containers and networks in the stack, but leaves the volumes intact.