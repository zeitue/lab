# Penpot

<img src="penpot.png" style="zoom:30%;" />

## Setup

```bash
# create binddn record, if using FreeIPA
./scripts/mkbinddn
# Add binddn to IPA instance, if using FreeIPA
./scripts/installbinddn
# generate variables
./scripts/mkenv
# create volumes, if using local
./scripts/mkvol
# setup the containers
./scripts/up
```



## Configuration

### Proxy

* Scheme `http`
* Forward Hostname / IP `penpot-frontend`
* Forward Port `80`


## Scripts

### installbinddn
Install the binddn record into the FreeIPA instance

### mkbinddn
Creates a binddn record for FreeIPA

### mkvol
Creates local volumes used for storage by the container

### mkenv
Creates environment variables for use by the containers, don't run this script more than once otherwise it'll overwrite the variables.env file

### up
Creates the containers, volumes, and networks in the stack

### update
Pull the latest container images for the stack

### upgrade
Pull the latest container images for the stack and then set the stack down and bring it back up with the new images

### down
Removes all containers and networks in the stack, but leaves the volumes intact.
