# Caddy 2

<img src="caddy.png" style="zoom:25%;" />

## Setup

```bash
# setup environment
./scripts/mkenv
# create the volumes, if local
./scripts/mkvol
# Create network
./scripts/mknet
# start master container
./scripts/up
```

## Scripts

### mkenv
Creates environment variables for use by the containers, don't run this script more than once otherwise it'll overwrite the .env file

### mknet
Creates proxy network

### mkvol
Creates volumes used for storage by the container

### up
Creates the containers, volumes, and networks in the stack

### update
Pull the latest container images for the stack

### upgrade
Pull the latest container images for the stack and then set the stack down and bring it back up with the new images

### down
Removes all containers and networks in the stack, but leaves the volumes intact.

### incudp
Patch the host settings to enable a larger UDP receive buffer size
