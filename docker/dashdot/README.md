# dash.
<img src="dashdot.png" style="zoom:100%;" />

## Setup

```bash
# generate variables
./scripts/mkenv
# setup the containers
./scripts/up
```

## Configuration

### Proxy

* Scheme `http`
* Forward Hostname / IP `dashdot`
* Forward Port `3001`

## Scripts
### mkenv
Creates environment variables for use by the containers, don't run this script more than once otherwise it'll overwrite the variables.env file

### up
Creates the containers, volumes, and networks in the stack

### update
Pull the latest container images for the stack

### upgrade
Pull the latest container images for the stack and then set the stack down and bring it back up with the new images

### down
Removes all containers and networks in the stack, but leaves the volumes intact.
