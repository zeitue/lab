# Vaultwarden
<img src="./vaultwarden.png" style="zoom: 8%;" />

## Setup
```bash
# generate variables
./scripts/mkenv
# create the volumes
./scripts/mkvol
# create binddn record
./scripts/mkbinddn
# Add binddn to IPA instance
./scripts/installbinddn
# Add groups
./scripts/installgroups
# setup the containers
./scripts/up
```



## Configuration
### Proxy

* Scheme `http`
* Forward Hostname / IP `vaultwarden`
* Forward Port `80`

## Scripts
### installbinddn
Install the binddn record into the FreeIPA instance

### mkbinddn
Creates a binddn record for FreeIPA

### mkvol
Creates volumes used for storage by the container

### installgroups
Install groups into the FreeIPA instance

### mkenv
Creates environment variables for use by the containers, don't run this script more than once otherwise it'll overwrite the .env file

### up
Creates the containers, volumes, and networks in the stack

### update
Pull the latest container images for the stack

### upgrade
Pull the latest container images for the stack and then set the stack down and bring it back up with the new images

### down
Removes all containers and networks in the stack, but leaves the volumes intact.

