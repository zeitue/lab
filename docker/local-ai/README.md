# LocalAI

<img src="./local-ai.png" style="zoom: 25%;" />


## Setup

```bash
# generate variables
./scripts/mkenv
# create the local volumes
./scripts/mkvol
# setup the containers
./scripts/up
```

## Scripts
### mkvol
Creates local volumes used for storage by the container

### mkenv
Creates environment variables for use by the containers, don't run this script more than once otherwise it'll overwrite the .env file

### up
Creates the containers, volumes, and networks in the stack

### update
Pull the latest container images for the stack

### upgrade
Pull the latest container images for the stack and then set the stack down and bring it back up with the new images

### down
Removes all containers and networks in the stack, but leaves the volumes intact.