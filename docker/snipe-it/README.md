



# Snipe-IT

<img src="./snipe-it.png" style="zoom: 100%;" />

## Setup

```bash
# generate variables
./scripts/mkenv
# create the local volumes, if used
./scripts/mkvol
# setup the containers
./scripts/up
```

## Configuration

### Initial

1. Setup your proxy
2. navigate to your `subdomain.domain.com`/setup
3. Click `Next: Create Database Tables`
4. Click `general.setup_migrations_create_user`
5. Fill in your 
   1. Site Name
   2. Default Currency
   3. Email Domain
   4. Email Format
   5. Then the super user's `First Name`, `Last Name`, `Email`, `Username`, and then set the Password

6. Click `Next: Save User`

### Proxy

* Scheme `http`
* Forward Hostname / IP `snipe-it`
* Forward Port `80`

### IPA Setup

```bash
# create binddn record
./scripts/mkbinddn
# Add binddn to IPA instance
./scripts/installbinddn
# Add groups to the FreeIPA instance
./scripts/installgroups

```

1. Login as the admin (superuser)
2. Click the gears in the upper right corner
3. Click on `LDAP`
4. Set the following (replace  dc=domain,dc=com with your own)
   * LDAP Integation `LDAP enabled`
   * LDAP Password Sync `Yes`
   * LDAP Server `ldap://ipa:389`
   * Use TLS `This should be checked only if you are running STARTTLS on your LDAP server.`
   * LDAP SSL certificate validation `Allow invalid SSL Certificate`
   * LDAP Bind Username `uid=snipe-it,cn=sysaccounts,cn=etc,dc=domain,dc=com`
   * LDAP Bind Password `The password in the snipe-it-binddn.update file`
   * Base Bind DN `cn=users,cn=accounts,dc=domain,dc=com`
   * LDAP Filter `&(objectClass=person)(memberof=cn=snipe-it,cn=groups,cn=accounts,dc=domain,dc=com)`
   * Username Field `uid`
   * Last Name `sn`
   * LDAP First Name `givenname`
   * LDAP Authentication query `uid=`
   * LDAP Version `3`
   * LDAP Employee Number `uid`
   * LDAP Email `mail`
   * LDAP Telephone Number `telephoneNumber`
   * LDAP Job Title `title`
5. Click `Save`



## Scripts

### installbinddn
Install the binddn record into the FreeIPA instance

### mkbinddn
Creates a binddn record for FreeIPA

### installgroups
Create groups in FreeIPA

### mkvol
Creates local volumes used for storage by the container

### mkenv
Creates environment variables for use by the containers, don't run this script more than once otherwise it'll overwrite the .env file

### up
Creates the containers, volumes, and networks in the stack

### update
Pull the latest container images for the stack

### upgrade
Pull the latest container images for the stack and then set the stack down and bring it back up with the new images

### down
Removes all containers and networks in the stack, but leaves the volumes intact.
