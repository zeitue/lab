# Nextcloud

## Setup
```bash
# generate variables
./scripts/mkenv
# create the volumes
./scripts/mkvol
# setup the containers
./scripts/up
```

## Configuration
### Proxy
#### Nextcloud
* Scheme `http`
* Forward Hostname / IP `nextcloud-aio-apache`
* Forward Port `11000`
* Custom

  ````nginx
    add_header X-Served-By $host;
    proxy_set_header Host $host;
    proxy_set_header X-Forwarded-Scheme $scheme;
    proxy_set_header X-Forwarded-Proto $scheme;
    proxy_set_header X-Forwarded-For $remote_addr;
    
    location /.well-known/carddav {
      return 301 $scheme://$host/remote.php/dav;
    }
    
    location /.well-known/caldav 
    { 
      return 301 $scheme://$host/remote.php/dav; 
    }
    
    location /.well-known/webdav 
    {
    return 301 $scheme://$host/remote.php/dav; 
    }
  ````

#### Setup
* Scheme `https`
* Forward Hostname / IP `nextcloud-aio-mastercontainer`
* Forward Port `8080`

### Initial
1. Navigate to nextcloud-setup.domain.com
2. Save your Nextcloud AIO setup password
3. Click `Open Nextcloud AIO login ↗`
4. Login with the password
5. Enter the full domain `nextcloud.domain.com` under New AIO instance
6. Click submit
7. Checkmark the addons you want
8. Click save changes
9. Set the timezone as `UTC`
10. Click submit
11. Click `Download and start containers`
12. set the `Backup and restore` to `nextcloud_aio_backupdir`
13. Click submit
14. Record the initial username and password
15. Click `Open your Nextcloud` 
16. Login with the recorded information

### IPA Setup
```bash
# Create binddn record
./scripts/mkbinddn
# Add binddn to IPA instance
./scripts/installbinddn
# Install groups to the IPA instance
./scripts/installgroups
# Install LDAP configuration in Nextcloud
./scripts/setup-ldap
```

### Authentik
TODO

#### Nextcloud
1. Login as the admin user
2. Click the avatar icon in the right corner
3. Click apps
4. locate `SSO & SAML authentication` then `Download and enable`
5. Navigate to `Administration settings` and click `SSO & SAML authentication` from the side bar
6. Click `Use built-in SAML authentication`
7. Fill in the following (replace domain.com with your own domain)
  * Global settings
    *  Only allow authentication if an account exists on some other backend (e.g. LDAP). `false`
    * Use SAML auth for the Nextcloud desktop clients (requires user re-authentication) `true`
    * Allow the use of multiple user back-ends (e.g. LDAP) `true`
  * General
    * Attribute to map the UID to. `http://schemas.goauthentik.io/2021/02/saml/username`
    * Optional display name of the identity provider `authentik` this can be anything you want
  * Service Provider Data
    * Name ID format `X509 subject name`
    * X.509 certificate of the Service Provider `copy the contents of Nextcloud_certificate.pem`
    * Private key of the Service Provider `copy the contents of Nextcloud_private_key.pem`
  * Identity Provider Data
    * Idenfier of the IdP entity `https://nextcloud.domain.com/apps/user_saml/saml/metadata`
    * URL Target of the IdP where SP will send the Authentication Request Message `https://authentik.domain.com/application/saml/nextcloud/sso/binding/redirect/`
    * URL Location of the IdP where SP will send the SLO Request `https://authentik.domain.com/if/session-end/nextcloud/`
    * Public X.509 certificate of the IdP `copy the contents of Nextcloud_certificate.pem`
  * Attribute mapping
    * Attribute to map the displayname to. `http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name`
    * Attribute to map the email address to. `http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress`
    * Attribute to map the user groups to. `http://schemas.xmlsoap.org/claims/Group`
  * Security settings
    * Indicates that the nameID of the \<samlp:logoutRequest\> sent by this SP will be encrypted. `true`
    * Indicates whether the \<samlp:AuthnRequest\> messages sent by this SP will be signed. [Metadata of the SP will offer this info] `true`
    * Indicates whether the  \<samlp:logoutRequest\> messages sent by this SP will be signed. `true`
    * Indicates whether the  \<samlp:logoutResponse\> messages sent by this SP will be signed. `true`
    * Whether the metadata should be signed. `true`
  * User filtering
    * Require membership in these groups, if any. `nextcloud`


### Email setup
1. Login as the admin user
2. Click the avatar icon in the right corner
3. Click `Administration settings`
4. Click `Basic settings` from the side bar
5. Fill in the following, changing what you need to
  * Send mode `SMTP`
  * Encryption `SSL`
  * From address `send_username` @ `domain.com`
  * Server address `smtp.gmail.com` : `465`
  * Authentication `true` Authentication required
  * Credentials `username@domain.com` `password`
6. Click Save
7. Test your settings by using the `Send email` button

## Scripts
### installbinddn
Install the binddn record into the FreeIPA instance

### mkbinddn
Creates a binddn record for FreeIPA

### installgroups
Install groups into the FreeIPA instance

### setup-ldap
Configure LDAP in Nextcloud

### mkvol
Creates local volumes used for storage by the container

### mkenv
Creates environment variables for use by the containers, don't run this script more than once otherwise it'll overwrite the variables.env file

### up
Creates the containers, volumes, and networks in the stack

### update
Pull the latest container images for the stack

### upgrade
Pull the latest container images for the stack and then set the stack down and bring it back up with the new images

### down
Removes all containers and networks in the stack, but leaves the volumes intact.
